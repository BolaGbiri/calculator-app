package com.sda.spring.Calculator;

import com.sda.spring.Calculator.operations.*;

public class CalculatorFactory {

    public Calculator getFactory(UserInput userInput) {
        if (String.valueOf(Operators.ADD).equalsIgnoreCase(userInput.getOperator())
                || Operators.PLUS.equals(userInput.getOperator())) {
            return new Addition();
        }
        if (String.valueOf(Operators.SUBTRACT).contains(userInput.getOperator().toUpperCase()) ||
                String.valueOf(Operators.MINUS).equalsIgnoreCase(userInput.getOperator())) {
            return new Subtraction();
        }
        if (String.valueOf(Operators.MULTIPLY).contains(userInput.getOperator().toUpperCase()) ||
                String.valueOf(Operators.TIMES).equalsIgnoreCase(userInput.getOperator())) {
            return new Multiplication();
        }
        if (String.valueOf(Operators.DIVIDE).contains(userInput.getOperator().toUpperCase()) ||
                String.valueOf(Operators.OVER).equalsIgnoreCase(userInput.getOperator())) {
            return new Division();
        }
        return null;
    }
}