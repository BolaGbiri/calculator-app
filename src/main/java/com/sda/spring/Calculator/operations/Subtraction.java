package com.sda.spring.Calculator.operations;

import com.sda.spring.Calculator.UserInput;

import java.math.BigDecimal;

public class Subtraction implements Calculator {

    @Override
    public BigDecimal doOperation(UserInput input) {
        BigDecimal difference = input.getInputA().subtract(input.getInputB());
        return difference;
    }
}
