package com.sda.spring.Calculator.operations;

import com.sda.spring.Calculator.Operators;

public class OperatorFactory {
    public Operators getOperator(String userInput) {
        if ('+' == userInput.charAt(0) || "add".equalsIgnoreCase(userInput) || "plus".equalsIgnoreCase(userInput)) {
            return Operators.ADD;
        }
        if ('-' == userInput.charAt(0) || "subtract".equalsIgnoreCase(userInput) || "minus".equalsIgnoreCase(userInput)) {
            return Operators.SUBTRACT;
        }
        if ('*' == userInput.charAt(0) || "multiply".equalsIgnoreCase(userInput) || "times".equalsIgnoreCase(userInput)) {
            return Operators.MULTIPLY;
        }
        if ('/' == userInput.charAt(0) || "divide".equalsIgnoreCase(userInput) || "over".equalsIgnoreCase(userInput)) {
            return Operators.DIVIDE;
        }
        return null;
    }
}
