package com.sda.spring.Calculator.operations;

import com.sda.spring.Calculator.Operators;
import com.sda.spring.Calculator.UserInput;

public class OperatorSignFactory {
    public char getOperatorSign(UserInput userInput) {
        if (String.valueOf(Operators.ADD).equalsIgnoreCase(userInput.getOperator())
                || String.valueOf(Operators.PLUS).equalsIgnoreCase(userInput.getOperator())) {
            return '+';
        }
        if (String.valueOf(Operators.SUBTRACT).contains(userInput.getOperator().toUpperCase()) ||
                String.valueOf(Operators.MINUS).equalsIgnoreCase(userInput.getOperator())) {
            return '-';
        }
        if (String.valueOf(Operators.MULTIPLY).contains(userInput.getOperator().toUpperCase()) ||
                String.valueOf(Operators.TIMES).equalsIgnoreCase(userInput.getOperator())) {
            return '*';
        }
        if (String.valueOf(Operators.DIVIDE).contains(userInput.getOperator().toUpperCase()) ||
                String.valueOf(Operators.OVER).equalsIgnoreCase(userInput.getOperator())) {
            return '/';
        }
        return ' ';
    }
}
