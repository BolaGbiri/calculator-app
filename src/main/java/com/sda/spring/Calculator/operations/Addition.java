package com.sda.spring.Calculator.operations;

import com.sda.spring.Calculator.UserInput;

import java.math.BigDecimal;

public class Addition implements Calculator {

    @Override
    public BigDecimal doOperation(UserInput userInput) {
        BigDecimal sum = userInput.getInputA().add(userInput.getInputB());
        return sum;
    }
}
