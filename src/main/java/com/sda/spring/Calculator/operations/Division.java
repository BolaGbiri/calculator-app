package com.sda.spring.Calculator.operations;

import com.sda.spring.Calculator.UserInput;

import java.math.BigDecimal;

public class Division implements Calculator {

    @Override
    public BigDecimal doOperation(UserInput userInput) {
        BigDecimal quotient = userInput.getInputA().divide(userInput.getInputB());
        return quotient;
    }
}
