package com.sda.spring.Calculator.operations;

import com.sda.spring.Calculator.UserInput;

import java.math.BigDecimal;

public class Multiplication implements Calculator {

    @Override
    public BigDecimal doOperation(UserInput userInput) {
        BigDecimal multiple = userInput.getInputA().multiply(userInput.getInputB());
        return multiple;
    }
}
