package com.sda.spring.Calculator.operations;

import com.sda.spring.Calculator.UserInput;

import java.math.BigDecimal;

public interface Calculator {
    BigDecimal doOperation(UserInput userInput);
}
