package com.sda.spring.Calculator;

import com.sda.spring.Calculator.operations.OperatorFactory;

import java.math.BigDecimal;

public class UserInput {
    private BigDecimal inputA;
    private BigDecimal inputB;
    private String operator;
    private String exitClause;
    private OperatorFactory factory = new OperatorFactory();

    public String getExitClause() {
        return exitClause;
    }

    public void setExitClause(String exitClause) {
        this.exitClause = exitClause;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        String value = String.valueOf(factory.getOperator(operator));
        this.operator = value;
    }

    public BigDecimal getInputA() {
        return inputA;
    }

    public void setInputA(BigDecimal inputA) {
        this.inputA = inputA;
    }

    public BigDecimal getInputB() {
        return inputB;
    }

    public void setInputB(BigDecimal inputB) {
        this.inputB = inputB;
    }
}
