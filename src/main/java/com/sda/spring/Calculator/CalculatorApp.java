package com.sda.spring.Calculator;

import com.sda.spring.Calculator.operations.Calculator;
import com.sda.spring.Calculator.operations.ExitOperation;
import com.sda.spring.Calculator.operations.OperatorSignFactory;

import java.math.BigDecimal;
import java.util.Scanner;

public class CalculatorApp {
    public static void main(String[] args) {
        UserInput input = new UserInput();
        Scanner scanner = new Scanner(System.in);
        Exceptions exceptions = new Exceptions();
        CalculatorFactory factory = new CalculatorFactory();
        OperatorSignFactory signFactory = new OperatorSignFactory();
        ExitOperation exitOperation = new ExitOperation();

        while (true) {
            System.out.println("Please enter a digit: ");
            input.setInputA(scanner.nextBigDecimal());
            scanner.nextLine();

            System.out.println("Please input your operation:'\n' Operations can be +,-,*,/ or add,plus,sub,minus,mul," +
                    "times,div,over.'\n'You can only do an operation one at a time.");
            input.setOperator(scanner.nextLine());

            System.out.println("Please enter another digit: ");
            input.setInputB(scanner.nextBigDecimal());
            scanner.nextLine();

            System.out.println("You have entered: " + input.getInputA() + " " + signFactory.getOperatorSign(input)
                    + " " + input.getInputB());
            if (exceptions.isAnyFieldEmpty(input)) {
                continue;
            }
            if (exceptions.checkOperation(factory, input)) {
                continue;
            }
            Calculator output = factory.getFactory(input);
            BigDecimal result = output.doOperation(input);

            System.out.println("The result is " + result);
            System.out.println("Do you wish to continue? type Y/N: ");
            input.setExitClause(scanner.nextLine());

            if (exitOperation.closeApp(input.getExitClause())) {
                return;
            }
        }
    }
}
