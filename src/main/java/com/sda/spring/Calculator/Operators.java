package com.sda.spring.Calculator;

public enum Operators {
    ADD,
    SUBTRACT,
    MULTIPLY,
    PLUS,
    MINUS,
    TIMES,
    OVER,
    DIVIDE
}
