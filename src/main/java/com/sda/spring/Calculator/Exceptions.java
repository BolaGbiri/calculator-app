package com.sda.spring.Calculator;

public class Exceptions {
    public boolean isAnyFieldEmpty(UserInput input) {
        if (input.getOperator().isEmpty() || "".equals(input.getOperator())) {
            System.out.println("Please declare a valid operation!");
            return true;
        }
        return false;
    }

    public boolean checkOperation(CalculatorFactory calculatorFactory, UserInput input) {
        if (calculatorFactory.getFactory(input) == null) {
            System.out.println("Your operation is not recognized");
            return true;
        }
        return false;
    }
}
